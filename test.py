class A:
    a = 0

    @classmethod
    def set_a(cls, a):
        cls.a = a

    def b(self):
        print(self.a)
a = A()

print(A.a)
A.set_a(2)
print(A.a)
a.b()
