import logging
import traceback


# This has to come before any other imports, as otherwise they might create
# A root logger instance or something before I set this.
class PolicyLogger(logging.Logger):
    def _log(self, level, msg, args, **kwargs):
        extra = {'url': kwargs.get('url', 'missing')}
        if kwargs.get('exc_info') is not None:
            msg = str(msg) + '\n' + traceback.format_exc()
        super(PolicyLogger, self)._log(level, msg, args, None, extra)

logging.setLoggerClass(PolicyLogger)
logging.basicConfig(level=logging.ERROR,
                    handlers=[logging.FileHandler('wayback.log'),
                              logging.StreamHandler()],
                    format='{levelname} - {name}: {url} : {msg}', style='{')

import pandas as pd
import hash_directory
import pathlib
from src.fetcher import Fetcher

pd.set_option('mode.chained_assignment', 'raise')




fetcher = Fetcher('/data/tmp/policies/', quarter=True, retry=True)
fetcher.set_years(['2016', '2017', '2018', '2019'])
fetcher.set_patterns(['datenschutz', 'datenschutz.html'])

out_path = pathlib.Path('/data/tmp/pol_dat/')
hash_directory.setup(out_path)

dat = pd.read_csv('feasibility.csv')
for ind, row in dat.iterrows():
    print(ind)
    pol_dat = fetcher.fetch(row.url)
    hash_directory.store(out_path, row.doc_id, pol_dat)