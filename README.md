# Policy Data

This script downloads snapsshots form the wayback machine.
Right now it requires a custom version of the wayback library, as the default does not properly implement ratelimits.

## Output format:

| Rowname | Datatype | Description|
| --- | --- | --- |
period | ISO Date Format - No time | The beginning of the period in which the snapshot falls. |
date | ISO Date Format | The date the snapshot was taken. |
digest | String | The digest of the snapshot. |
original | String | The original url indexed. |
html | String | The html source returned from wayback machine.|

