import logging
import pathlib
import pandas as pd
import numpy as np
from typing import List, Optional
from urllib.parse import urlparse
from datetime import datetime, timezone
import itertools
import re

import src.api
from src.utils import *


class Fetcher:
    """
    This class exposes the functions used to fetch the mementos.

    :param: cache: The directory in which to look for and store the local copies.
        If None is provided, all memorios will be fetched from the Wayback Machine and none stored.
    :type cache: str
    :param years: The years for which to look for memorios.
        Since there is no good conversion from R datetime objects to python datetime objects,
        this function expects a list of strings.
        For details see :func:`~Fetcher.set_years`.
    :type years: List[str]
    :param quarter: If set the code searches for quarterly downloads.
    :type quarter: bool
    :param retry: Should I retry all mementos that raised an error?
    :type retry: bool, default is False
    """

    _names = ['period', 'date', 'digest', 'original', 'html']
    _good_codes = ['200', '301', '302', '-']
    _years = None
    _patterns = None
    _correct_scheme = re.compile(r'^https?://')
    _any_scheme = re.compile(r'^[a-z]*://')
    _periods: List[datetime] = []

    def __init__(self, cache: str = None, years: List[str] = None, quarter: bool = False, retry: bool=False):
        self._logger = logging.getLogger(__name__)
        self._out_path = pathlib.Path(cache) if cache is not None else None
        self._quarter = quarter
        if years is not None:
            self.set_years(years)

        self._api = src.api.API(self._out_path, retry_all=retry, slow=True)

    def set_years(self, years: List[str]) -> bool:
        """
        This method sets the years for which the snapshots will be fetched.
        Dateformat: 'YYYY-MM-DD HH:MM:SS', any date in the year is accepted.
        Since always using this format is cumbersome, the function also accepts only the year.
        If the string is only four numbers long, the function will assume this is an abbreviated
        form and add the rest.

        If the arguments are not of the proper type, an error is written to the logfile.

        :param years: A list of the years for which to fetch data.
        :type years: List[str]
        :return: A boolean indicating whether the function was successful.
        """
        if type(years) is not list:
            self._logger.error('years are not a List')
            return False
        if not all([type(i) is str for i in years]):
            self._logger.error('Not all patterns are string')
            return False
        self._years = [datetime.strptime(d if len(d) != 4 else d + '-01-01 00:00:00',
                                         '%Y-%m-%d %H:%M:%S') for d in years]
        if self._quarter:
            self._periods = list(itertools.chain.from_iterable(
                [[datetime(year=year.year, month=q * 3 + 1, day=1) for q in range(4)]
                 for year in self._years]
            ))
        else:
            self._periods = self._years
        return True

    def set_patterns(self, patterns: List[str]) -> bool:
        """
        This method allows the user to set a list of patterns.
        When patterns are set, the fetch function goes through all patterns
        to find the Mementos, if the primary url did not find it.

        :param patterns: The patterns
        :type patterns: List[str]
        :return: A boolean indicating success.
        """
        if type(patterns) is not list:
            self._logger.error('patterns is not a List')
            return False
        if not all([type(i) is str for i in patterns]):
            self._logger.error('Not all patterns are string')
            return False
        self._patterns = patterns
        return True

    def fetch(self, url: str) -> Optional[pd.DataFrame]:
        """
        This function is exposed to the R code. Therefore, I need to check all types.

        Right now the url must contain a scheme, or else the internal urlparsing fails.

        :param url: The url that should be fetched
        :type url: str
        :return: A DataFrame with the results, None on error.
        :rtype: :class:`pd.DataFrame` or None
        """

        if type(url) is not str:
            self._logger.error(f'Wrong type supplied.', url=url)
            return None
        if self._correct_scheme.match(url) is None:
            if self._any_scheme.match(url) is not None:
                self._logger.error(f'Wrong url type: {self._any_scheme.match[0]}', url=url)
                return None
            url = 'https://' + url

        if self._years is None:
            self._logger.error('Years not set')
            return None

        results = self._build_empty_frame()
        self._logger.debug('started url', url=url)
        self._fetch_url(url, results)
        # TODO 2021-05-15 lion: I should check here that I don't run a pattern twice.
        # But since this has little costs, it's not high upon my priority list.
        if results.html.isnull().any() and self._patterns is not None:
            for pattern in self._patterns:
                self._logger.debug(f'started pattern {pattern}', url=url)
                pattern_url = 'https://' + urlparse(url).netloc + '/' + pattern
                self._fetch_url(pattern_url, results)
                if not results.html.isnull().any():
                    break
        return results

    def _fetch_url(self, url: str, results: Optional[pd.DataFrame] = None):
        """
        Fetches the results for a given url. If a results frame was provided, the function fetches
        only for the missing years. If not it tries all years.

        :param url: The url that should be worked with.
        :param results: A dataframe, resulting from previous queries. If provided, it will be updated.
        :return: None, the information is written to the results frame.
        """
        years: List[datetime] = [self._periods[i] for i in np.where(results.html.isnull())[0]]
        cdx = self._api.cdx(url, self._after(years))
        if cdx is None:
            # Error! handling should come later
            return results
        cdx['ok'] = cdx.statuscode.apply(lambda x: x in self._good_codes)
        self._update_results(cdx, results, url)
        return results

    def _update_results(self, cdx_frame: pd.DataFrame, results: pd.DataFrame, url: str):
        """
        This function selects the first snapshot for each year.
        I only select mementos that have a 200 OK response. I think I should
        expand this, based on how the Wayback Machine handles redirect.

        The method goes through all rows of the dataframe and tries to find a matching cdx entry for each column.
        If I find such an entry, I write the url and the date to the result dataframe.

        :param cdx_frame: The cdx response from the wayback machine.
        :param results: The Dataframe which contains the results.
        :param url: The url, it is needed for the fetch function.
        """

        cdx_frame['year'] = cdx_frame.timestamp.apply(lambda x: x.year)
        cdx_frame['quarter'] = cdx_frame.timestamp.apply(lambda x: x.quarter)

        for ind, row in results.iterrows():
            if pd.isnull(row.original):
                # look for a matching snap
                matching = (cdx_frame.year == row.period.year) & cdx_frame.ok
                # If I work with quarters I need to also match these.
                matching = matching & (cdx_frame.quarter == row.period.quarter)
                if not matching.any():
                    continue
                cdx = cdx_frame.iloc[matching.argmax(), :]
                # Write this cdx entry to the row.
                results.loc[ind, 'date'] = cdx.timestamp
                results.loc[ind, 'digest'] = cdx.digest
                results.loc[ind, 'original'] = cdx.original
                html = self._api.fetch(cdx, url)
                if html is not None:
                    results.loc[ind, 'html'] = html

    @staticmethod
    def _after(years: List[datetime]) -> datetime:
        """
        This method gets the latest date for which I need a snapshot.

        WARNING: This method might not work properly with quarters.

        :return: A data, any CDX fresher than this date is considered good enough.
        """
        year = max([d.year for d in years]) + 1
        after = datetime.fromisoformat(f'{year}-01-01T00:00:00')
        return after.replace(tzinfo=timezone.utc)

    def _build_empty_frame(self) -> pd.DataFrame:
        tdat = pd.DataFrame(columns=self._names,
                            index=range(len(self._periods)))
        tdat['period'] = self._periods
        return tdat
