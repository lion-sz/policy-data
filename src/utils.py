from datetime import datetime


def time_to_str(t: datetime) -> str:
    return t.strftime('%Y%m%d%H%M%S')


def str_to_time(t: str) -> datetime:
    return datetime.strptime(t, '%Y%m%d%H%M%S')
