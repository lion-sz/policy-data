import logging
import pathlib
import wayback
import pandas as pd
from urllib.parse import urlparse
from datetime import datetime, timezone
from typing import Optional
import re
import sys
import time

from src.utils import *


URL_TEMPLATE = 'http://web.archive.org/web/{}id_/{}'


class API:
    """
    This class manages the cdx api.
    It has two public functions, fetch the cdx response and fetch a snapshot.

    I don't communicate with the wayback machine directly, but rather work with the

    It also manages the local backups.

    :param cache: The path where the cached responses are to be stored.
    :type cache: `pathlib.Path`
    :param retry_all: Should the api retry all mementos that gave an error in a previous run?
        If set, then previous error files are deleted and the memento is tried again.
    :type retry_all: bool
    :param slow: If set the wayback session is limited to one memento call every two seconds.
        This is implemented so that if I download with existing CDX, I don't go to quick on the wayback machine.
    :type slow: bool
    """
    reg_timestamp = re.compile('http://web.archive.org/web/([0-9]*)id_/*')
    names = ['urlkey', 'timestamp', 'original', 'mimetype', 'statuscode', 'digest', 'length', 'raw_url', 'view_url']

    def __init__(self, cache: pathlib.Path, retry_all: bool = False, slow: bool = False):
        self.cache = cache
        self._retry_all = retry_all
        if slow:
            self.session = wayback.WaybackSession(retries=2, backoff=10, timeout=30, memento_calls_per_second=0.5)
        else:
            self.session = wayback.WaybackSession(retries=2, backoff=10, timeout=30)
        self.client = wayback.WaybackClient(self.session)
        self.logger = logging.getLogger(__name__)

    def _build_path(self, url: str) -> Optional[pathlib.Path]:
        """
        Since I don't want to work with a deep directory tree,
        I use a directory for the host and one subdirectory for each path,
        where I replace slashes with underscores.
        :param url: The input url
        :return: The path
        """
        if self.cache is None:
            return None
        parsed = urlparse(url)
        tmp = self.cache / parsed.netloc
        if parsed.path != '' and parsed.path != '/':
            # remove leading slashes
            suffix = parsed.path[1:] if parsed.path[0] == '/' else parsed.path
            tmp = tmp / suffix.replace('/', '_')
        return tmp
 
    def cdx(self, url, after: datetime) -> Optional[pd.DataFrame]:
        """
        Fetch all cdx entries and make a local backup.

        :param url: The url
        :param after: This is used to decide, if I need a newer version of the cdx api.
        :type after: UTC timestamp
        :return: A Dataframe representing the cdx entries. The timestamp column is a datetime object.
            None, when there are not available mementos.
        """
        path = self._build_path(url)
        if path is not None:
            # Check if there already is a local version of the CDX.
            # This is the case if the path contains the touched file exists and the timestamp
            # is more recent then the after cutoff.
            path.mkdir(parents=True, exist_ok=True)
            touched_file = path / 'touched'
            if touched_file.exists():
                touched = datetime.fromisoformat(touched_file.read_text().strip()).replace(tzinfo=timezone.utc)
                if touched > after:
                    # I have no need to fetch a new cdx. I can simply use the old one.
                    cdx_file = path / 'CDX.txt'
                    if not cdx_file.exists():
                        return None
                    self.logger.debug('using cached CDX response.', url=url)
                    frame = pd.read_csv(cdx_file, sep=' ', dtype=str)
                    self._update_cdx(frame, cdx_file)
                    if 'raw_url' not in frame.columns:
                        sys.exit('failed')
                    frame.timestamp = frame.timestamp.apply(str_to_time)
                    return frame

        # Download a new CDX.
        try:
            response = self.client.search(url)
            records = [(x.key, x.timestamp, x.url, x.mime_type, x.status_code,
                        x.digest, x.length, x.raw_url, x.view_url) for x in response]
            if len(records) == 0:
                if path is not None:
                    (path / 'touched').write_text(
                        datetime.utcnow().isoformat(sep=' ', timespec='seconds'))
                return None
            frame = pd.DataFrame.from_records(records)
            frame.columns = self.names
            frame.timestamp = frame.timestamp.apply(lambda x: x.to_pydatetime())
            frame.statuscode = frame.statuscode.astype(str)
        except wayback.exceptions.RateLimitError as err:
            self.logger.err(err, url=url)
            if err.retry_after is not None:
                time.sleep(err.retry_after)
            else:
                time.sleep(60)
            return None
        except Exception as err:
            self.logger.exception(err, url=url)
            return None

        # If local enabled write the touched and the CDX file.
        # I need to cast the datetime object to the CDX timestamp format
        if path is not None:
            (path / 'touched').write_text(
                datetime.utcnow().isoformat(sep=' ', timespec='seconds'))
            out_frame = frame.copy(deep=True)
            out_frame.timestamp = out_frame.timestamp.apply(time_to_str)
            out_frame.to_csv(path / 'CDX.txt', sep=' ')
            self.logger.debug('dumped CDX to cache.', url=url)
        return frame

    def _update_cdx(self, cdx: pd.DataFrame, path: pathlib.Path):
        """
        The R cdx results contain an extra column 'year' and is missing the raw_url key.
        Therefore, I change this and write the updated cdx to the file.
        :param cdx: The cdx read in.
        :type path: The path from which I read the cdx result.
        :return: None, the frame is altered.
        """
        if 'date' in cdx.columns:
            del cdx['date']
        if 'raw_url' not in cdx.columns:
            cdx['raw_url'] = cdx.apply(lambda x: URL_TEMPLATE.format(x.timestamp, x.original),
                                       axis=1)
        cdx.to_csv(path, sep=' ')

    def fill(self, result: pd.DataFrame, url: str):
        result['html'] = ''
        for ind, row in result.iterrows():
            html = self.fetch(row, url)
            if html is not None:
                result.loc[ind, 'html'] = html

    def fetch(self, row: pd.core.series.Series, url: str) -> Optional[str]:
        """
        This function fetches a snapshot, using if possible a local copy.

        :param row: The row from the cdx response.
        :param url: I need the url to properly get the file path.
        :return: A string, None on error.
        """
        # First check for a cached response:
        path = self._build_path(url)
        if path is not None:
            cached = path / (row.digest + '.htm')
            if cached.exists():
                try:
                    response = cached.read_bytes()
                    self.logger.debug(f'using cached response {row.digest}', url=url)
                    return response.decode(errors='replace')
                except Exception as err:
                    self.logger.exception(err)
            # But also check for a cached error message.
            else:
                cache = path / (row.digest + '.err')
                if cache.exists():
                    if not self._retry_all:
                        self.logger.debug(f'using cached error {row.digest}', url=url)
                        return None
                    else:
                        cache.unlink()
                        self.logger.debug(f'retrying cached error {row.digest}', url=url)

        try:
            response = self.client.get_memento(row.raw_url, exact=False, target_window=60)
            if path is not None:
                (path / (row.digest + '.htm')).write_bytes(response.content)
            self.logger.debug(f'fetching memento {row.digest}', url=url)
            return response.content.decode(errors='replace')
        except wayback.exceptions.RateLimitError as err:
            self.logger.exception(err, url=url)
            if err.retry_after is not None:
                time.sleep(err.retry_after)
            else:
                time.sleep(60)
            return None
        except Exception as err:
            self.logger.exception(err, url=url)
            # and if path is set write the error to an .err file.
            if path is not None:
                (path / (row.digest + '.err')).write_text(str(err))
            return None

